package pe.com.bcp.bcpchallenge.business.entity

import com.google.gson.JsonObject

class Exchange(val base: String,val date: String,val rates:JsonObject)