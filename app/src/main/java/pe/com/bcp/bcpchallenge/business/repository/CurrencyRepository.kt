package pe.com.bcp.bcpchallenge.business.repository

import io.reactivex.Observable
import pe.com.bcp.bcpchallenge.business.entity.Exchange
import java.util.*

interface CurrencyRepository {

    fun getExchanges(base:String): Observable<Exchange>
}