package pe.com.bcp.bcpchallenge.ui.main

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import pe.com.bcp.bcpchallenge.R
import pe.com.bcp.bcpchallenge.business.entity.Currency
import pe.com.bcp.bcpchallenge.ui.currency.CurrencyListActivity

class MainActivity : AppCompatActivity(), MainContract.View {

    companion object {
        const val GET_CURRENCY_REQUEST_CODE = 33
    }

    private lateinit var mainPresenter: MainContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        changeImageV.setOnClickListener { onChangeCurrency() }
        computeOperationButton.setOnClickListener { onComputeOperation(getFromValue()) }
        fromTv.setOnLongClickListener {
            open(it, true)
        }
        toTextV.setOnLongClickListener {
            open(it, false)
        }

        mainPresenter = MainPresenterImpl(this)

    }

    private fun open(it: View?, isFrom: Boolean): Boolean {
        openCurrencyList((it as TextView).text as String, isFrom)
        return true
    }

    private fun openCurrencyList(currency: String, isFrom: Boolean) {
        val intent = Intent(this, CurrencyListActivity::class.java).apply {
            putExtra(CurrencyListActivity.EXTRA_CURRENCY, currency)
            putExtra(CurrencyListActivity.EXTRA_IS_FROM, isFrom)
        }
        startActivityForResult(intent, GET_CURRENCY_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GET_CURRENCY_REQUEST_CODE) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                val (currency) = data?.getSerializableExtra(CurrencyListActivity.EXTRA_CURRENCY) as Currency
                val isFrom = data.getBooleanExtra(CurrencyListActivity.EXTRA_IS_FROM, false)
                when (isFrom) {
                    true -> {
                        setFromCurrencyName(currency)
                    }
                    else ->
                        setToCurrencyName(currency)
                }
                mainPresenter.updateCurrency(currency, isFrom)
                mainPresenter.getExchangesBy(currency)
            }
        }
    }

    override fun onChangeCurrency() {
        mainPresenter.toggleCurrency()
    }

    override fun setFromCurrencyName(nameFrom: String) {
        fromTv.text = nameFrom
    }

    override fun setToCurrencyName(nameTo: String) {
        toTextV.text = nameTo
    }

    override fun setBuyAndSellValues(buy: Float, sell: Float) {
        val buyAndSell = """
            Compra: $buy | Venta: $sell
        """.trimIndent()
        buySellTextV.text = buyAndSell
    }

    override fun getFromValue(): Float = fromEditT.text.toString().trim().toFloat()

    override fun onComputeOperation(value: Float) {
        mainPresenter.compute(value)
    }

    override fun setToCurrencyValue(valueText: String) {
        toEditT.setText(valueText)
    }
}
