package pe.com.bcp.bcpchallenge.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_currency.view.*
import pe.com.bcp.bcpchallenge.R
import pe.com.bcp.bcpchallenge.business.entity.Currency
import kotlin.collections.ArrayList

class CurrencyListAdapter constructor(
    private val base: String,
    private val currencyList: ArrayList<Currency>,
    private val currencyItemListener: CurrencyItemCallback
) : RecyclerView.Adapter<CurrencyListAdapter.CurrencyItemViewH>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyItemViewH {
        return CurrencyItemViewH(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_currency,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = currencyList.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CurrencyItemViewH, position: Int) {
        val currency = currencyList[position]
        holder.itemView.setOnClickListener {
            currencyItemListener.onItemCurrencyPressed(currencyList[position])
        }
        holder.nameTextV.text = currency.name
        holder.exchangeTextV.text = """ 1 $base = ${currency.value} ${currency.currency}"""
    }

    inner class CurrencyItemViewH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTextV: TextView = itemView.nameTextV
        val exchangeTextV: TextView = itemView.exchangeRateTextV
    }

    interface CurrencyItemCallback {
        fun onItemCurrencyPressed(currency: Currency)
    }
}