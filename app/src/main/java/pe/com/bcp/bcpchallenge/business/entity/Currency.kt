package pe.com.bcp.bcpchallenge.business.entity

import java.io.Serializable

data class Currency(var currency: String, val name:String , var value: Float) : Serializable