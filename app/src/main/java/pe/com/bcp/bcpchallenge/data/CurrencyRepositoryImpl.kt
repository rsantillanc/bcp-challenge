package pe.com.bcp.bcpchallenge.data

import io.reactivex.Observable
import pe.com.bcp.bcpchallenge.business.entity.Exchange
import pe.com.bcp.bcpchallenge.business.repository.CurrencyRepository
import pe.com.bcp.bcpchallenge.data.net.ExchangeRateApi

class CurrencyRepositoryImpl : CurrencyRepository {

    private val api: ExchangeRateApi by lazy {
        ExchangeRateApi.create()
    }

    override fun getExchanges(base: String): Observable<Exchange> =
        api.getExchangesAsync(base)
}