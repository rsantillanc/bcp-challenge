package pe.com.bcp.bcpchallenge.ui.main

interface MainContract {
    interface View {
        fun onChangeCurrency()
        fun setFromCurrencyName(nameFrom: String)
        fun setToCurrencyName(nameTo: String)
        fun setBuyAndSellValues(buy: Float, sell: Float)
        fun getFromValue(): Float
        fun onComputeOperation(value:Float)
        fun setToCurrencyValue(valueText: String)
    }

    interface Presenter {
        fun toggleCurrency()
        fun compute(fromValue: Float)
        fun getExchangesBy(base: String)
        fun updateCurrency(currency: String, isFrom:Boolean)
    }
}