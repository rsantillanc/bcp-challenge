package pe.com.bcp.bcpchallenge.ui.main

import android.util.Log
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import pe.com.bcp.bcpchallenge.business.repository.CurrencyRepository
import pe.com.bcp.bcpchallenge.data.CurrencyRepositoryImpl
import kotlin.random.Random

class MainPresenterImpl constructor(private val mainView: MainContract.View) :
    MainContract.Presenter {

    private val repository: CurrencyRepository by lazy {
        CurrencyRepositoryImpl()
    }
    private val composite: CompositeDisposable by lazy {
        CompositeDisposable()
    }

    private var currencyNamePrimary = "USD"
    private var currencyNameSecondary = "PEN"
    private var currencyNameTemp = ""
    private var valueToCompute = 0.0f

    init {
        getExchangesBy(currencyNamePrimary)
    }

    override fun toggleCurrency() {
        currencyNameTemp = currencyNamePrimary
        currencyNamePrimary = currencyNameSecondary
        currencyNameSecondary = currencyNameTemp

        mainView.setFromCurrencyName(currencyNamePrimary)
        mainView.setToCurrencyName(currencyNameSecondary)


        mainView.setBuyAndSellValues(Random.nextFloat()*100, Random.nextFloat()*100)

        getExchangesBy(currencyNamePrimary)
    }

    override fun compute(fromValue: Float) {
        mainView.setToCurrencyValue("${valueToCompute * fromValue}")
    }

    override fun getExchangesBy(base: String) {
        composite.add(
            repository.getExchanges(base)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    valueToCompute = result.rates.get(currencyNameSecondary).asFloat

                    Log.d("Result", "Exchanges ${Gson().toJson(result)}")
                }, { error ->
                    Log.d("Result", "Error ${error.message}")
                })
        )
    }

    override fun updateCurrency(currency: String, isFrom:Boolean) {
        if (isFrom) currencyNamePrimary = currency else currencyNameSecondary = currency
    }


}