package pe.com.bcp.bcpchallenge.ui.currency

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_currency_list.*
import pe.com.bcp.bcpchallenge.R
import pe.com.bcp.bcpchallenge.business.entity.Currency
import pe.com.bcp.bcpchallenge.ui.adapter.CurrencyListAdapter

class CurrencyListActivity : AppCompatActivity(), CurrencyListAdapter.CurrencyItemCallback {

    companion object {
        const val EXTRA_CURRENCY = "#EXTRA_CURRENCY"
        const val EXTRA_IS_FROM = "#EXTRA_IS_FROM"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currency_list)
        val adapter = intent?.getStringExtra(EXTRA_CURRENCY)?.let {
            CurrencyListAdapter(
                it, arrayListOf(
                    Currency("USD", "United State", 20.22F),
                    Currency("JPY", "Japan", 15.22F),
                    Currency("GBP", "United Kingdom", 10.22F),
                    Currency("CHF", "Switzerland", 2.22F),
                    Currency("CAD", "Canada", 62.22F)
                ), this
            )
        }
        currencyRecyclerV.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        currencyRecyclerV.adapter = adapter
    }

    override fun onItemCurrencyPressed(currency: Currency) {
        val result = Intent()
        result.putExtra(EXTRA_CURRENCY, currency)
        result.putExtra(EXTRA_IS_FROM, intent?.getBooleanExtra(EXTRA_IS_FROM, false))
        setResult(Activity.RESULT_OK, result)
        finish()
    }
}
