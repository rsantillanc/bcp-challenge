package pe.com.bcp.bcpchallenge.data.net

import io.reactivex.Observable
import pe.com.bcp.bcpchallenge.business.entity.Exchange
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface ExchangeRateApi {

    @GET("{currency}")
    fun getExchangesAsync(
        @Path("currency") currency: String
    ): Observable<Exchange>

    /**
     * Companion object to create the GithubApiService
     */
    companion object Factory {
        private const val URL_BASE=  "https://api.exchangerate-api.com/v4/latest/"

        fun create(): ExchangeRateApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(URL_BASE)
                .build()

            return retrofit.create(ExchangeRateApi::class.java)
        }
    }
}